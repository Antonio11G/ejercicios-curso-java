import java.util.ArrayList;
import java.util.List;

public class Lista {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Alumno student = null;
		ArrayList<Alumno> listStudent = new ArrayList<Alumno>(20);
		for (int i = 0; i < 20; i++) {
			student=new Alumno();
			if(i%2==0) {
				student.setNombre("Juan"+i+"a");
			}else {
				student.setNombre("Juan"+i);
			}
			student.setApellidoPaterno("Juancito"+i);
			student.setApellidoMaterno("Juanelo"+i);
			student.setNombreCurso("Introduccion a java");
			listStudent.add(student);
		}
		listStudent.forEach((Alumno a)->System.out.println(a));
		System.out.println(listStudent.size());
		System.out.println("\n******************* Alumnos con letra A al final del nombre");
		listStudent.forEach((Alumno a)->{
			if(a.getNombre().charAt(a.getNombre().length()-1)=='a')
				System.out.println(a);
			});
		System.out.println("\n******************* Primeros 5 Alumnos");
		listStudent.stream().limit(5).forEach((Alumno a)->{
			//if(a.getNombre().charAt(a.getNombre().length()-1)=='a')
				System.out.println(a);
			});
	}
}
