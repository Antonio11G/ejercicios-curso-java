package gameImplements;

import java.util.Optional;

import interfaces.Gameable;
import interfaces.Playeable;
import interfaces.Soundable;

public class PrincipalGame {
	
	static String song1 =null,gameName="qe";
	static double playerPosX=4.5, playerPosY=3.1;	
	public static void main (String[] args) {
		
		Playeable p1 = (playerPosX, playerPosY)->{
			System.out.println("Players before: "+playerPosX+", "+ playerPosY);
			playerPosX++;playerPosY++;
			System.out.println("Players after: "+playerPosX+", "+ playerPosY);
		};
		
		p1.walk(playerPosX, playerPosY);
		Gameable game = () ->{
			p1.setGameName(gameName);
		};
		game.startGame();
		
		Soundable song = (song1)-> {
			Optional<String> cancion = Optional.of(song1);	
			if(!cancion.isPresent()) {
				song1="newSoundTrack1";
			}
			System.out.println("The song will be: "+song1);
		};
		song.playMusic(song1);
	}
	
}
