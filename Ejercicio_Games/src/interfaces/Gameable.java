package interfaces;

public interface Gameable {
	public void startGame();
	
	default void setGameName(String name) {
		System.out.println("The Name is: "+name);
	}
}
