package interfaces;

import java.util.Optional;

public interface Playeable {
	
	public void walk(double x, double y);
	
	default void setGameName(String name) {
		Optional<String> nameGame = Optional.of(name);
		nameGame.orElseThrow( ()->{
			throw new NullPointerException();
			}
		);
		System.out.println("The Game is: "+nameGame.get());
	}
}
