package interfaces;

public interface Soundable {
	public void playMusic(String song);
	default void setGameName(String name) {
		System.out.println("The Song is: "+name);
	}
}
